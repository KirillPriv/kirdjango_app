from django.contrib import admin
from .models import Author, Category, Tag, Article


# class AuthorInLine(admin.TabularInline):
#     model = Author


class ArticleAdmim(admin.ModelAdmin):
    list_display = ['id', 'title', 'content', 'author', 'category', 'pub_date']
    list_filter = ['title', 'author', 'category']
    search_fields = ['title']
    # inlines = [AuthorInLine]


# Register your models here.
admin.site.register(Author)
admin.site.register(Article, ArticleAdmim)
admin.site.register(Category)
admin.site.register(Tag)
