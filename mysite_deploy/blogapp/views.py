from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.syndication.views import Feed
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic import ListView, CreateView, DetailView
from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema, OpenApiResponse
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.viewsets import ModelViewSet

from blogapp.models import Article

from blogapp.serializers import ArticleSerializer


class MainView(View):  # переход на главную страницу после авторизации на ShopLoginView
    def get(self, request):
        return render(request, 'blogapp/base.html')


class ArticleListView(LoginRequiredMixin, ListView):
    queryset = (
        Article.objects.filter(pub_date__isnull=False)
            .order_by('-pub_date')
            .defer('content')
            .select_related("author")
            .prefetch_related("tags")
    )

class ArticleDetailView(DetailView):
    model = Article

class LatestArticlesFeed(Feed):
    title = 'Blog articles (latest)'
    description = 'Updates on changes and addition blog articles'
    link = reverse_lazy('blogapp:article_list')

    def items(self):
        return(
        Article.objects.filter(pub_date__isnull=False)
            .order_by('-pub_date')
            .defer('content')
            .select_related("author")
            .prefetch_related("tags")[:5]
        )

    def item_title(self, item: Article):
        return item.title

    def item_description(self, item: Article):
        return item.content[:200]

    # def item_link(self, item: Article):
    #     return reverse('blogapp:article-detail', kwargs={'pk': item.pk})

class ArticleCreateView(CreateView):
    model = Article
    fields = ['title', 'content', 'author', 'category', 'tags']
    success_url = reverse_lazy('blogapp:article_list')


@extend_schema(description="Article views CRUD")
class ArticleViewSet(ModelViewSet):
    """
    Набор представлений для действий над Article
    Полный CRUD для сущеностей статей
    """
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    filter_backends = [
        SearchFilter,
        DjangoFilterBackend,
        OrderingFilter,
    ]
    search_fields = ['title', 'author']
    filterset_fields = [
        'title',
        'content',
        'author',
        'category',
    ]
    ordering_fields = [
        'title',
        'author',
        'category',
    ]

    @extend_schema(
        summary='Get one article by ID',
        description='Retrieves **article**, returns 404 if not found',
        responses={
            200: ArticleSerializer,
            404: OpenApiResponse(description='Empty response, article by id not found'),
        }
    )
    def retrieve(self, *args, **kwargs):
        return super().retrieve(*args, **kwargs)

