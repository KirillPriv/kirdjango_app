from django.db import models


# Create your models here.
from django.urls import reverse


class Author(models.Model):
    """
    Модель Product представляет представляет автора статьи

    """
    name = models.CharField(max_length=100, db_index=True)
    bio = models.TextField(null=False, blank=True)

    def __str__(self):
        return f"Author(pk={self.pk}, name={self.name!r})"


class Category(models.Model):
    """
     Модель Category представляет категорию статьи

    """
    name = models.CharField(max_length=40, db_index=True)

    def __str__(self):
        return f"Category(pk={self.pk}, name={self.name!r})"


class Tag(models.Model):
    """
    Модель Tag представляет тэг, который можно назначить статье

    """
    name = models.CharField(max_length=20, db_index=True)

    def __str__(self):
        return f"Tag(pk={self.pk}, name={self.name!r})"


class Article(models.Model):
    """
    Модель Article представляет представляет статью

    """

    class Meta:
        ordering = ["title", "author"]

    title = models.CharField(max_length=200, db_index=True)
    content = models.TextField(null=False, blank=True)
    pub_date = models.DateTimeField(null=True, blank=True)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag)

    def __str__(self):
        return f"Article(pk={self.pk}, title={self.title}, author{self.author!r})"

    def get_absolute_url(self):
        return reverse('blogapp:article-detail', kwargs={'pk': self.pk})