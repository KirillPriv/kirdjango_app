from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import (
    MainView,
    ArticleListView,
    ArticleCreateView,
    ArticleViewSet,
    ArticleDetailView,
    LatestArticlesFeed,

)

app_name = "blogapp"

routers = DefaultRouter()
routers.register('articles', ArticleViewSet)


urlpatterns = [
    path("", MainView.as_view(), name="main_view"),
    path("articles/create/", ArticleCreateView.as_view(), name="article_create"),
    path("articles/", ArticleListView.as_view(), name="article_list"),
    path("articles/<int:pk>/", ArticleDetailView.as_view(), name="article-detail"),
    path("articles/latest/feed/", LatestArticlesFeed(), name="article-feed")
    # path('api/', include(routers.urls))
]