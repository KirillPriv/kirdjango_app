from django.contrib.auth.models import Group, User
from rest_framework import serializers

from .models import Article

class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model =Article
        fields = [
            'pk',
            'title',
            'content',
            'author',
            'category',
            'tags',
        ]