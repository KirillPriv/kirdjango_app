"""
В этом модуле лежат различные наборы представлений.

Разные view интернет магазина: по товарам, заказам и т.д.
"""

import logging

from random import random

from csv import DictWriter

from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group, User
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin, UserPassesTestMixin
from timeit import default_timer

from django.contrib.auth.models import Group
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.syndication.views import Feed
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, reverse, get_object_or_404
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView, TemplateView

from rest_framework.decorators import api_view, action
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView, ListCreateAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.parsers import MultiPartParser

from .serializers import GroupSerializer, ProductSerializer, OrderSerializer

from drf_spectacular.utils import extend_schema, OpenApiResponse

from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator
from django.core.cache import cache

from .models import Product, Order, Profile
from .common import save_csv_products

# log = logging.getLogger(__name__)


class ShopIndexView(View):
    # @method_decorator(cache_page(60 * 2))
    def get(self, request: HttpRequest) -> HttpResponse:
        products = [
            ('Laptop', 1999),
            ('Desktop', 2999),
            ('Smartphone', 999),
        ]
        context = {
            "time_running": default_timer(),
            "products": products,
        }
        # log.debug('Products for shop index %s', products)
        # log.info('Rendering shop index')
        print('shop index contex', context)
        return render(request, 'shopapp/shop-index.html', context=context)


@cache_page(60 * 2)
def get_cookie_view(request: HttpRequest) -> HttpResponse:
    value = request.COOKIES.get('fizz', 'default value')
    return HttpResponse(f'Cookie value: {value!r} + {random()}')


def groups_list(request: HttpRequest):
    context = {
        "groups": Group.objects.prefetch_related('permissions').all(),
    }
    return render(request, 'shopapp/groups-list.html', context=context)


# def products_list(request: HttpRequest):
#     context = {
#         "products": Product.objects.all(),
#     }
#     return render(request, 'shopapp/products-list.html', context=context)

# def orders_list(request: HttpRequest):
#     context = {
#         "orders": Order.objects.select_related("user").prefetch_related("products").all(),
#     }
#     return render(request, 'shopapp/orders-list.html', context=context)

class OrderListView(LoginRequiredMixin, ListView):
    queryset = (
        Order.objects
            .select_related("user")
            .prefetch_related("products")
    )
    # template_name = 'shopapp/order_list.html'
    # model = Order
    # context_object_name = 'object_list'


class UserOrdersListView(LoginRequiredMixin, ListView):
    template_name = 'shopapp/order_list_user.html'
    model = Order
    owner = None

    def get_queryset(self):
        user_id = self.kwargs.get('user_id')
        self.owner = get_object_or_404(User, pk=user_id)
        return super().get_queryset().filter(user=self.owner)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['owner'] = self.owner
        return context


class OrderDetailView(PermissionRequiredMixin, DetailView):
    permission_required = ['view_order']
    template_name = 'shopapp/orders-detail.html'
    model = Order
    context_object_name = 'order'


class OrderCreateView(CreateView):
    model = Order
    fields = ['user', 'delivery_address', 'products', 'promocode', 'receipt']
    success_url = reverse_lazy('shopapp:orders_list')


class OrderUpdateView(UpdateView):
    model = Order
    fields = ['user', 'delivery_address', 'products', 'promocode', 'receipt']
    # template_name = 'order_update_form'
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse(
            'shopapp:orders_detail',
            kwargs={'pk': self.object.pk},
        )


class OrderDeleteView(DeleteView):
    model = Order
    success_url = reverse_lazy('shopapp:orders_list')


# class OrderDetailView(View):
#     def get(self, request: HttpRequest, pk: int) -> HttpResponse:
#         order = get_object_or_404(Order, pk=pk)
#         context = {
#             'order': order
#         }
#         return render(request, 'shopapp/products-detail.html', context=context)

class OrdersDataExportView(View):

    def get(self, request: HttpRequest, user_id) -> JsonResponse:
        owner = get_object_or_404(User, id=user_id)
        cache_key = f'orders_data_user_id:{user_id}>export'
        orders_data = cache.get(cache_key)
        if orders_data is None:
            orders = Order.objects.order_by('pk').filter(user=owner)
            orders_data = [
                {
                    'user': order.user.username,
                    'delivery_address': order.delivery_address,
                    'promocode': order.promocode,
                    'created_at': order.created_at,
                    'products': [product.name for product in order.products.all()],
                }
                for order in orders
            ]
            cache.set(cache_key, orders_data, 180)
        return JsonResponse({'orders': orders_data})


class ProductsListView(ListView):
    template_name = 'shopapp/products-list.html'
    # model = Product
    queryset = Product.objects.filter(
        archived=False
    )  # Для того чтобы в списке отображались только объекты с archived=False
    context_object_name = 'products'


class ProductsDataExportView(View):
    def get(self, request: HttpRequest) -> JsonResponse:
        cache_key = 'products_data>export'
        products_data = cache.get(cache_key)
        if products_data is None:
            products = Product.objects.order_by('pk').all()
            products_data = [
                {
                    'pk': product.pk,
                    'name': product.name,
                    'price': product.price,
                    'archived': product.archived,
                }
                for product in products
            ]
            cache.set(cache_key, products_data, 300)
        elem = products_data[0]
        name = elem['name']
        print('nsme:', name)
        return JsonResponse({'products': products_data})


# class ProductDetailView(View):
#     def get(self, request: HttpRequest, pk: int) -> HttpResponse:
#         product = get_object_or_404(Product, pk=pk)
#         context = {
#             'product': product
#         }
#         return render(request, 'shopapp/products-detail.html', context=context)

class ProductDetailView(DetailView):
    template_name = 'shopapp/products-detail.html'
    model = Product
    context_object_name = 'product'


class ProductCreateView(UserPassesTestMixin, CreateView):
    def test_func(self):
        # return self.request.user.groups.filter(name='secret-group').exists() # проверка на существование пользователя в группе
        return self.request.user.is_superuser

    model = Product
    fields = ['name', 'price', 'description', 'discount', 'preview']
    success_url = reverse_lazy('shopapp:products_list')


class ProductUpdateView(UpdateView):
    model = Product
    fields = ['name', 'price', 'description', 'discount', 'preview']
    # template_name = либо указать имя шаблона либо имя суффикса ниже
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse(
            'shopapp:products_detail',
            kwargs={'pk': self.object.pk},
        )


class ProductDeleteView(DeleteView):
    model = Product
    success_url = reverse_lazy('shopapp:products_list')

    def form_valid(self, form):  # данный метод переопределяет удаление и делает его не полным
        success_url = self.get_success_url()
        self.object.archived = True
        self.object.save()
        return HttpResponseRedirect(success_url)


class LatestProductsFeed(Feed):
    title = 'Products (latest)'
    description = 'Updates on changes and addition products'
    link = reverse_lazy('shopapp:products_list')

    def items(self):
        return (
            Product.objects.filter(archived=False)
                .order_by('-created_at')[:5]
        )

    def item_title(self, item: Product):
        return item.name

    def item_description(self, item: Product):
        return item.description[:200]


class MainView(View):  # переход на главную страницу после авторизации на ShopLoginView
    def get(self, request):
        return render(request, 'shopapp/base.html')


class ShopLoginView(LoginView):
    template_name = 'shopapp/login.html'
    redirect_authenticated_user = True


class ShopLogoutView(LogoutView):
    template_name = 'shopapp/logout.html'


class AboutUserView(TemplateView):
    template_name = 'shopapp/about-user.html'


class RegisterUserView(CreateView):
    form_class = UserCreationForm
    template_name = 'shopapp/register.html'
    success_url = reverse_lazy('shopapp:about_user')

    def form_valid(self, form):
        response = super().form_valid(form)
        Profile.objects.create(user=self.object)  # связка юзера с формой (расширение формы user)
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password1')
        user = authenticate(
            self.request,
            username=username,
            password=password
        )
        login(request=self.request, user=user)
        return response


class GroupsListViewGeneric(ListCreateAPIView):  # Данный код упрощает логику
    # queryset = Group.objects.all()
    queryset = User.objects.all()
    serializer_class = GroupSerializer


@extend_schema(description='Product views CRUD')
class ProductViewSet(ModelViewSet):
    """
    Набор представлений для действий над Product
    Полный CRUD для сущеностей товара
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = [
        SearchFilter,
        DjangoFilterBackend,
        OrderingFilter,
    ]
    search_fields = ['name', 'description']
    filterset_fields = [
        'name',
        'description',
        'price',
        'discount',
        'archived',
    ]
    ordering_fields = [
        'name',
        'price',
        'discount',
    ]

    @extend_schema(
        summary='Get one product by ID',
        description='Retrieves **product**, returns 404 if not found',
        responses={
            200: ProductSerializer,
            404: OpenApiResponse(description='Empty response, product by id not found'),
        }
    )
    def retrieve(self, *args, **kwargs):
        return super().retrieve(*args, **kwargs)

    @method_decorator(cache_page(60 * 2))
    def list(self, *args, **kwargs):  # переопределение метода для кеширования этого класса
        # print('hello products list')
        return super().list(*args, **kwargs)

    @action(methods=['get'], detail=False)
    def download_csv(self, request: Request):
        response = HttpResponse(content_type="text/csv")
        filename = 'products-export-csv'
        response["Content-Disposition"] = f"attachment; filename={filename}"
        queryset = self.filter_queryset(self.get_queryset())
        fields = [
            'name',
            'description',
            'price',
            'discount',
        ]
        queryset = queryset.only(*fields)
        writer = DictWriter(response, fieldnames=fields)
        writer.writeheader()

        for product in queryset:
            writer.writerow({
                field: getattr(product, field)
                for field in fields
            })

        return response

    @action(
        detail=False,
        methods=['post'],
        parser_classes=[MultiPartParser],
    )
    def upload_csv(self, request: Request):
        products = save_csv_products(
            request.FILES['file'].file,
            encoding=request.encoding,
        )
        serializer = self.get_serializer(products, many=True)
        return Response(serializer.data)


class OrderViewSet(ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_backends = [
        SearchFilter,
        DjangoFilterBackend,
        OrderingFilter,
    ]
    search_fields = ['user', 'delivery_address']
    filterset_fields = [
        'user',
        'delivery_address',
        'promocode',
        'created_at',
        'products',
    ]
    ordering_fields = [
        'user',
        'delivery_address',
        'products',
    ]
