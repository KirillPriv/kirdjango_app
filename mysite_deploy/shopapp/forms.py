from django import forms
from django.forms import ModelForm
from .models import Product


# class ProductForm(ModelForm):
#     def __init__(self, *args, **kwargs):
#         self.is_authenticated = kwargs.pop('is_authenticated')
#         super().__init__(*args, **kwargs)
#
#     class Meta:
#         model = Product
#         fields = ['name', 'description', 'price']

class AuthForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class CSVImportForm(forms.Form):
    csv_file = forms.FileField()