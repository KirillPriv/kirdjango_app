from django.urls import path, include
from rest_framework.routers import DefaultRouter
# from django.views.decorators.cache import cache_page

from .views import (
    ShopIndexView,
    groups_list,
    get_cookie_view,
    #products_list,
    #orders_list,
    OrderListView,
    UserOrdersListView,
    OrderDetailView,
    OrderCreateView,
    OrderUpdateView,
    OrderDeleteView,
    OrdersDataExportView,
    ProductDetailView,
    ProductsListView,
    ProductCreateView,
    ProductUpdateView,
    ProductDeleteView,
    ProductsDataExportView,
    LatestProductsFeed,
    MainView,
    ShopLoginView,
    ShopLogoutView,
    AboutUserView,
    RegisterUserView,
    GroupsListViewGeneric,
    ProductViewSet,
    OrderViewSet
)

app_name = "shopapp"

routers = DefaultRouter()
routers.register('products', ProductViewSet)
routers.register('orders', OrderViewSet)

urlpatterns = [
    path("cookie/get/", get_cookie_view, name="main_view"),
    # path("index/", cache_page(60*3)(ShopIndexView.as_view()), name="index"),
    path("index/", ShopIndexView.as_view(), name="index"),
    path("", MainView.as_view(), name="main_view"),
    path("groups/", groups_list, name="groups_list"),
    #path("products/", products_list, name="products_list"),
    path("products/", ProductsListView.as_view(), name="products_list"),
    path("products/create/", ProductCreateView.as_view(), name="product_create"),
    path("products/<int:pk>/", ProductDetailView.as_view(), name="products_detail"),
    path("products/<int:pk>/update-product/", ProductUpdateView.as_view(), name="product_update"),
    path("products/<int:pk>/archive/", ProductDeleteView.as_view(), name="product_archive"),
    path("products/export/", ProductsDataExportView.as_view(), name="products_export"),
    path("products/latest/feed/", LatestProductsFeed(), name="product-feed"),
    #path("orders/", orders_list, name="orders_list"),
    path("orders/", OrderListView.as_view(), name="orders_list"),
    path('users/<int:user_id>/orders/', UserOrdersListView.as_view(), name="user_orders_list"),
    path("orders/create/", OrderCreateView.as_view(), name="order_create"),
    path("orders/<int:pk>/", OrderDetailView.as_view(), name="orders_detail"),
    path("orders/<int:pk>/update-order/", OrderUpdateView.as_view(), name="order_update"),
    path("orders/<int:pk>/archive/", OrderDeleteView.as_view(), name="order_delete"),
    path("users/<int:user_id>/orders/export/", OrdersDataExportView.as_view(), name="orders_export"),
    path('login/', ShopLoginView.as_view(), name='login_form'),
    path('logout/', ShopLogoutView.as_view(), name='logout_form'),
    path('about-user/', AboutUserView.as_view(), name='about_user'),
    path('register-user/', RegisterUserView.as_view(), name='register_user'),
    path('groups-generic/', GroupsListViewGeneric.as_view(), name='groups-generic'),
    path('api/', include(routers.urls))
]