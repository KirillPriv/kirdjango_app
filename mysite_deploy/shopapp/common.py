from csv import DictReader
from io import TextIOWrapper

from .models import Product, Order


def save_csv_products(file, encoding):
    csv_file = TextIOWrapper(
        file,
        encoding=encoding,
    )

    reader = DictReader(csv_file)

    products = [
        Product(**row)
        for row in reader
    ]
    Product.objects.bulk_create(products)
    return products


def save_csv_orders(file, encoding):
    csv_file = TextIOWrapper(
        file,
        encoding=encoding,
    )

    reader = DictReader(csv_file)

    # orders = [
    #     Order(**row)
    #     for row in reader
    # ]
    # Order.objects.bulk_create(orders)

    orders = []
    for item in reader:
        products = item.pop('products')
        order = Order.objects.create(**item)
        for id_product in products.split(','):
            product = Product.objects.filter(id=id_product).first()
            if product:
                order.products.add(product)
        orders.append(order)

    return orders