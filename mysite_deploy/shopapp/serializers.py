from django.contrib.auth.models import Group, User
from rest_framework import serializers

from .models import Product, Order


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['pk', 'username']


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            'pk',
            'name',
            'description',
            'price',
            'discount',
            'created_at',
            'archived',
            'preview',
        ]


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = [
            'pk',
            'user',
            'delivery_address',
            'promocode',
            'created_at',
            'products',
            'receipt',
        ]
