from random import choices
from string import ascii_letters

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from django.conf import settings

from shopapp.models import Product


class MainViewTestCase(TestCase):
    def test_mainview(self):
        response = self.client.get(reverse('shopapp:main_view'))
        self.assertEqual(response.status_code, 200)


class ProductCreateViewTestCase(TestCase):
    # def setUp(self) -> None:
    #     self.product_name = ''.join(choices(ascii_letters, k=10))
    #     Product.objects.filter(name=self.product_name).delete()
    def test_create_product(self):
        response = self.client.post(
            reverse('shopapp:product_create'),
            {
                'name': 'Table',
                'price': '123.45',
                'description': 'A good table',
                'discount': '10',
                'preview': ''
            }
        )
        # self.assertRedirects(response, reverse('shopapp:products_list'))
        self.assertEqual(response.status_code, 302)
        # self.assertTrue(
        #     Product.objects.filter(name=self.product_name).exists()
        # )


class ProductDetailViewTestCase(TestCase):  # тест проверка страницы детали продукта
    @classmethod
    def setUpClass(cls):  # класс-метод для создания продукта
        cls.product = Product.objects.create(name='Best Product')

    # def setUp(self) -> None:
    #     self.product = Product.objects.create(name='Best Product')
    @classmethod
    def tearDownClass(cls):  # класс-метод для удаления продукта после прохождения теста
        cls.product.delete()

    # def tearDown(self) -> None:
    #     self.product.delete()

    def test_get_product(
            self):  # метод проверяет наличие страницы и сверяет статус запроса
        response = self.client.get(
            reverse('shopapp:products_detail', kwargs={'pk': self.product.pk})
        )
        self.assertEqual(response.status_code, 200)

    def test_get_product_and_check_content(
            self):  # метод проверяет полученную страницу на содержание имени продукта
        response = self.client.get(
            reverse('shopapp:products_detail', kwargs={'pk': self.product.pk})
        )
        self.assertContains(response, self.product.name)


class ProductListViewTestCase(TestCase):
    fixtures = [
        'products-fixture.json',
    ]

    def test_products(self):
        # response = self.client.get(reverse('shopapp:products_list')) # 1-ый вариант теста
        # for product in Product.objects.filter(archived=False).all():
        #     self.assertContains(response, product.name)

        # response = self.client.get(reverse('shopapp:products_list')) # 2-ой вариант теста более подробная проверка
        # products = Product.objects.filter(archived=False).all()
        # products_ = response.context['products']
        # for p, p_ in zip(products, products_):
        #     self.assertEqual(p.pk, p_.pk)

        response = self.client.get(reverse('shopapp:products_list'))  # 3-ий вариант теста
        self.assertQuerysetEqual(
            qs=Product.objects.filter(archived=False).all(),
            values=(p.pk for p in response.context['products']),
            transform=lambda p: p.pk,
        )

        self.assertTemplateUsed('shopapp/products-list.html')  # проверка использованного шаблона


class OrdersListViewTestCase(TestCase):
    @classmethod
    def setUpClass(cls):  # класс-метод для создания пользователя
        cls.credentials = dict(username='bob_test', password='qwerty')
        cls.user = User.objects.create_user(**cls.credentials)

    @classmethod
    def tearDownClass(cls):  # класс-метод для удаления юзера после прохождения теста
        cls.user.delete()

    def setUp(self) -> None:
        self.client.login(**self.credentials)

    def test_orsers_view(self):
        response = self.client.get(reverse('shopapp:orders_list'))
        self.assertContains(response, 'Orders')

    def test_orders_view_not_autentificated(self):
        self.client.logout()
        response = self.client.get(reverse('shopapp:orders_list'))
        # self.assertRedirects(response, '/shop/login/')
        self.assertEqual(response.status_code, 302)  # проверка статуса переадресации
        self.assertIn('/shop/login/', response.url)  # проверка входит ли строчка в url


class ProductExportViewTestCase(TestCase):
    fixtures = [
        'products-fixture.json',
    ]

    def test_get_products_view(self):
        response = self.client.get(reverse('shopapp:products_export'))
        self.assertEqual(response.status_code, 200)
        products = Product.objects.order_by('pk').all()
        expected_data = [
            {
                'pk': product.pk,
                'name': product.name,
                'price': str(product.price),
                'archived': product.archived,
            }
            for product in products
        ]
        products_data = response.json()
        self.assertEqual(
            products_data['products'],
            expected_data,
        )

